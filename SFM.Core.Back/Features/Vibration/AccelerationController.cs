using Microsoft.AspNetCore.Mvc;
using SFM.Core.Back.CassandraDB;
using SFM.Core.Back.Features.Vibration.Models;

namespace SFM.Core.Back.Features.Vibration;

public class AccelerationController(CassandraConnector cassandra) : Controller
{
    [HttpPost("acceleration")]
    public async Task InsertAcceleration([FromBody] AccelerationAddModel model)
    {
        var statement = cassandra.PreparedStatements[CassandraCommand.InsertAcceleration]
            .Bind("imu", model.Timestamp, model.X, model.Y, model.Z);
        
        await cassandra.Session!.ExecuteAsync(statement);
    }
}