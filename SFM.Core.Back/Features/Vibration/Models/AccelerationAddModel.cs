using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Back.Features.Vibration.Models;

public class AccelerationAddModel
{
    [Required]
    public long Timestamp { get; set; }
    
    [Required]
    public int X { get; set; }
    
    [Required]
    public int Y { get; set; }
    
    [Required]
    public int Z { get; set; }
}