using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using SFM.Core.Back.CassandraDB;
using SFM.Core.Back.Features.Plug.Models;

namespace SFM.Core.Back.Features.Plug;

public class PlugController(CassandraConnector cassandra) : Controller
{
    [HttpPost("power")]
    public async Task InsertPower([FromBody] PowerAddModel model)
    {
        var data = new PowerSaveModel
        {
            SensorId = "power",
            Timestamp = model.Timestamp,
            Power = float.Parse(model.Power, CultureInfo.InvariantCulture)
        };
        
        var statement = cassandra.PreparedStatements[CassandraCommand.InsertPower]
            .Bind(data.SensorId, data.Timestamp, data.Power);
        
        await cassandra.Session!.ExecuteAsync(statement);
    }
    
    
    [HttpPost("energy")]
    public async Task InsertEnergy([FromBody] EnergyAddModel model)
    {
        var data = new EnergySaveModel
        {
            SensorId = "energy",
            Timestamp = model.Timestamp,
            Energy = int.Parse(model.Energy, CultureInfo.InvariantCulture)
        };
        
        var statement = cassandra.PreparedStatements[CassandraCommand.InsertEnergy]
            .Bind(data.SensorId, data.Timestamp, data.Energy);
        
        await cassandra.Session!.ExecuteAsync(statement);
    }
}