namespace SFM.Core.Back.Features.Plug.Models;

public class PowerSaveModel
{
    public required string SensorId;
    public long Timestamp { get; set; }
    public float Power { get; set; }
}