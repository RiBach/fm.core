using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Back.Features.Plug.Models;

public class PowerAddModel
{
    [Required]
    public long Timestamp { get; set; }
    
    [Required]
    public required string Power { get; set; }
}