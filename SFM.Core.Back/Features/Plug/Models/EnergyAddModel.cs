using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Back.Features.Plug.Models;

public class EnergyAddModel
{
    [Required]
    public long Timestamp { get; set; }
    
    [Required]
    public required string Energy { get; set; }
}