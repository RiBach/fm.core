namespace SFM.Core.Back.Features.Plug.Models;

public class EnergySaveModel
{
    public required string SensorId;
    
    public long Timestamp { get; set; }
    
    public int Energy { get; set; }
    
}