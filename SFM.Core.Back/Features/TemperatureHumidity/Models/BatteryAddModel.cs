using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Back.Features.TemperatureHumidity.Models;

public class BatteryAddModel
{
    [Required]
    public required string SensorId { get; set; }
    
    [Required]
    public long Timestamp { get; set; }
    
    [Required]
    public required string Battery { get; set; }
}