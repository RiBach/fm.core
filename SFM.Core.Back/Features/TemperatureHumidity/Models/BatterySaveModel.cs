namespace SFM.Core.Back.Features.TemperatureHumidity.Models;

public class BatterySaveModel
{
    public required string Identifier { get; set; }
    public required string SensorId { get; set; }
    
    public long Timestamp { get; set; }
    
    public short Battery { get; set; }
}