using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Back.Features.TemperatureHumidity.Models;

public class TemperatureAddModel
{
    [Required]
    public required string SensorId { get; set; }
    
    [Required]
    public long Timestamp { get; set; }
    
    [Required]
    public required string Temperature { get; set; }
}