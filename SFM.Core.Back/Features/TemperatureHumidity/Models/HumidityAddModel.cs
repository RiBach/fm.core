using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Back.Features.TemperatureHumidity.Models;

public class HumidityAddModel
{
    [Required]
    public required string SensorId { get; set; }
    
    [Required]
    public long Timestamp { get; set; }
    
    [Required]
    public required string Humidity { get; set; }
}