using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using SFM.Core.Back.CassandraDB;
using SFM.Core.Back.Features.TemperatureHumidity.Models;

namespace SFM.Core.Back.Features.TemperatureHumidity;

public class TemperatureHumidityController(CassandraConnector cassandra) : Controller
{
    [HttpPost("temperature")]
    public async Task InsertTemperature([FromBody] TemperatureAddModel model)
    {
        var data = new TemperatureSaveModel
        {
            Identifier = $"{EnvironmentSensorIdentifierDictionary.GetIdentifier[model.SensorId]}_temperature",
            SensorId = model.SensorId,
            Timestamp = model.Timestamp,
            Temperature = float.Parse(model.Temperature, CultureInfo.InvariantCulture)
        };
        
        var statement = cassandra.PreparedStatements[CassandraCommand.InsertTemperature]
            .Bind(data.Identifier, data.SensorId, data.Timestamp, data.Temperature);
        
        await cassandra.Session!.ExecuteAsync(statement);
    }
    
    
    [HttpPost("humidity")]
    public async Task InsertHumidity([FromBody] HumidityAddModel model)
    {
        var data = new HumiditySaveModel
        {
            Identifier = $"{EnvironmentSensorIdentifierDictionary.GetIdentifier[model.SensorId]}_humidity",
            SensorId = model.SensorId,
            Timestamp = model.Timestamp,
            Humidity = float.Parse(model.Humidity, CultureInfo.InvariantCulture)
        };
        
        var statement = cassandra.PreparedStatements[CassandraCommand.InsertHumidity]
            .Bind(data.Identifier, data.SensorId, data.Timestamp, data.Humidity);
        
        await cassandra.Session!.ExecuteAsync(statement);
    }
    
    
    [HttpPost("battery")]
    public async Task InsertBattery([FromBody] BatteryAddModel model)
    {
        var data = new BatterySaveModel
        {
            Identifier = $"{EnvironmentSensorIdentifierDictionary.GetIdentifier[model.SensorId]}_battery",
            SensorId = model.SensorId,
            Timestamp = model.Timestamp,
            Battery = short.Parse(model.Battery, CultureInfo.InvariantCulture)
        };
        
        var statement = cassandra.PreparedStatements[CassandraCommand.InsertBattery]
            .Bind(data.Identifier, data.SensorId, data.Timestamp, data.Battery);
        
        await cassandra.Session!.ExecuteAsync(statement);
    }
}