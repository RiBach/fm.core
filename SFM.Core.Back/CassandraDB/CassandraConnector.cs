using Cassandra;
using ISession = Cassandra.ISession;

namespace SFM.Core.Back.CassandraDB;

public class CassandraConnector(IConfiguration conf)
{
    public ISession? Session;

    public readonly Dictionary<CassandraCommand, PreparedStatement> PreparedStatements = new();
    
    private readonly Cluster _cluster = Cluster.Builder()
        .AddContactPoint(conf["Cassandra:ContactPoint"])
        .WithDefaultKeyspace(conf["Cassandra:Keyspace"])
        .Build();
    
    
    public void Connect()
    {
        while (Session is null)
        {
            try
            {
                Console.WriteLine($@"{DateTime.Now.TimeOfDay:hh\:mm\:ss} : Connecting to Cassandra...");
                Session = _cluster.ConnectAndCreateDefaultKeyspaceIfNotExists();
            }
            catch (Exception)
            {
                Console.WriteLine($@"{DateTime.Now.TimeOfDay:hh\:mm\:ss} : Cassandra not ready");
                Thread.Sleep(TimeSpan.FromSeconds(15));
            }
        }

        Console.WriteLine("Migrating schema...");
        Session.Execute(
            $"create table if not exists {conf["Cassandra:Keyspace"]}.environment (" +
            $"identifier text," +
            $"timestamp timestamp," +
            $"sensor_id text," +
            $"temperature float," +
            $"humidity float," +
            $"battery smallint," +
            $"primary key (identifier, timestamp))" +
            $"with clustering order by (timestamp desc) and default_time_to_live = 31536000");
        
        Session.Execute(
            $"create table if not exists {conf["Cassandra:Keyspace"]}.energy (" +
            $"identifier text," +
            $"timestamp timestamp," +
            $"power float," +
            $"energy int," +
            $"primary key (identifier, timestamp))" +
            $"with clustering order by (timestamp desc) and default_time_to_live = 31536000");
        
        Session.Execute(
            $"create table if not exists {conf["Cassandra:Keyspace"]}.compressor (" +
            $" identifier text," +
            $"timestamp timestamp," +
            $"acc_x int," +
            $"acc_y int," +
            $"acc_z int," +
            $"primary key (identifier, timestamp))" +
            $"with clustering order by (timestamp desc) and default_time_to_live = 3600");

        foreach (var command in Enum.GetValues<CassandraCommand>())
        {
            PreparedStatements[command] = Session.PrepareAsync(CommandDictionary.Commands[command]).Result;
        }
        
        Console.WriteLine($@"{DateTime.Now.TimeOfDay:hh\:mm\:ss} : Connected to Cassandra");
    }
}