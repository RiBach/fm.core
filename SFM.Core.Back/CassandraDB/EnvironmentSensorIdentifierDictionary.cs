namespace SFM.Core.Back.CassandraDB;

public static class EnvironmentSensorIdentifierDictionary
{
    /// <summary>
    /// Dictionary to get the identifier for the environment sensor.
    /// Key: SensorId
    /// </summary>
    public static readonly Dictionary<string, string> GetIdentifier = new()
    {
        { "esp01_a1", "upper" },
    };
}