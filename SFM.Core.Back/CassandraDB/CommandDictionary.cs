namespace SFM.Core.Back.CassandraDB;

public enum CassandraCommand
{
    InsertPower,
    InsertEnergy,
    InsertAcceleration,
    InsertTemperature,
    InsertHumidity,
    InsertBattery
}

public static class CommandDictionary
{
    public static readonly Dictionary<CassandraCommand, string> Commands = new()
    {
        {
            CassandraCommand.InsertPower, "INSERT INTO energy (identifier, timestamp, power) VALUES (?, ?, ?)"
        },
        {
            CassandraCommand.InsertEnergy, "INSERT INTO energy (identifier, timestamp, energy) VALUES (?, ?, ?)"
        },
        {
            CassandraCommand.InsertAcceleration, "INSERT INTO compressor (identifier, timestamp, acc_x, acc_y, acc_z) VALUES (?, ?, ?, ?, ?)"
        },
        {
            CassandraCommand.InsertTemperature, "INSERT INTO environment (identifier, sensor_id, timestamp, temperature) VALUES (?, ?, ?, ?)"
        },
        {
            CassandraCommand.InsertHumidity, "INSERT INTO environment (identifier, sensor_id, timestamp, humidity) VALUES (?, ?, ?, ?)"
        },
        {
            CassandraCommand.InsertBattery, "INSERT INTO environment (identifier, sensor_id, timestamp, battery) VALUES (?, ?, ?, ?)"
        }
    };
}