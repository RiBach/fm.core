create keyspace sfmbeta with replication = {'class':'SimpleStrategy', 'replication_factor':1};

-- drop table environment;
CREATE TABLE sfmbeta.environment (
    identifier text,
    timestamp timestamp,
    sensor_id text,
    temperature smallint,
    humidity smallint,
    battery smallint,
  PRIMARY KEY (identifier, timestamp)
) WITH CLUSTERING ORDER BY (timestamp DESC) and default_time_to_live = 31536000;

-- drop table energy;
create table sfmbeta.energy (
    identifier text,
    timestamp timestamp,
    power float,
    energy int,
    primary key ( identifier, timestamp )
) with clustering order by (timestamp desc) and default_time_to_live = 31536000;

-- drop table compressor;
create table sfmbeta.compressor (
    identifier text,
    timestamp timestamp,
    acc_x int,
    acc_y int,
    acc_z int,
    hall int,
    primary key ( identifier, timestamp )
) with clustering order by (timestamp desc) and default_time_to_live = 3600;