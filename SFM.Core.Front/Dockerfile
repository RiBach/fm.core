FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app

# http
EXPOSE 5144

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["SFM.Core.Front/SFM.Core.Front.csproj", "SFM.Core.Front/"]
RUN dotnet restore "SFM.Core.Front/SFM.Core.Front.csproj"
COPY . .
WORKDIR "/src/SFM.Core.Front"
RUN dotnet build "SFM.Core.Front.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "SFM.Core.Front.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SFM.Core.Front.dll"]