namespace SFM.Core.Front.Features.Plug.Models;

public class EnergyResponseModel
{
    public decimal? KiloWattHours { get; set; }
    public int? WattMinutes { get; set; }
}