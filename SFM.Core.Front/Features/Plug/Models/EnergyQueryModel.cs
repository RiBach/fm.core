using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Front.Features.Plug.Models;

public class EnergyQueryModel
{
    [Required] 
    public string StartTime { get; set; } = default!;
    
    [Required]
    public string EndTime { get; set; } = default!;
}