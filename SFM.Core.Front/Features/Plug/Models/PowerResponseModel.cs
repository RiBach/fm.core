namespace SFM.Core.Front.Features.Plug.Models;

public class PowerResponseModel
{
    public DateTime? Timestamp { get; set; }
    public float? Value { get; set; }
}