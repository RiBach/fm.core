using Microsoft.AspNetCore.Mvc;
using SFM.Core.Front.CassandraDB;
using SFM.Core.Front.Features.Plug.Models;

namespace SFM.Core.Front.Features.Plug;

public class PlugController(CassandraConnector cassandra) : Controller
{
    [HttpGet("range/power")]
    public async Task<IActionResult> GetPowerByRange([FromQuery] PowerQueryModel model)
    {
        var statement = Queries.PowerByRange(model.StartTime, model.EndTime);
        
        var resultSet = await cassandra.Session!.ExecuteAsync(statement);
        var rows = resultSet.GetRows().ToList();
        
        if (rows is [null])
        {
            return NotFound();
        }
        
        var data = rows.Select(row => new PowerResponseModel
        {
            Timestamp = row.GetValue<DateTime>("timestamp"),
            Value = row.GetValue<float>("power")
        }).ToList();
        
        return Ok(data);
    }
    
    [HttpGet("latest/power")]
    public async Task<IActionResult> GetLatestPower()
    {
        var statement = Queries.PowerLatest();
        
        var resultSet = await cassandra.Session!.ExecuteAsync(statement);
        var row = resultSet.GetRows().FirstOrDefault();
        
        if (row == null)
        {
            return NotFound();
        }
        
        var data = new PowerResponseModel
        {
            Timestamp = row.GetValue<DateTime>("timestamp"),
            Value = row.GetValue<float>("power")
        };
        
        return Ok(data);
    }
    
    
    [HttpGet("range/energy")]
    public async Task<IActionResult> GetEnergyByRange([FromQuery] PowerQueryModel model)
    {
        var statement = Queries.EnergyByRange(model.StartTime, model.EndTime);
        
        var resultSet = await cassandra.Session!.ExecuteAsync(statement);
        var row = resultSet.GetRows().FirstOrDefault();

        if (row == null)
        {
            return NotFound();
        }
        
        var data = new EnergyResponseModel
        {
            KiloWattHours = Math.Round((row.GetValue<int>("latest") - row.GetValue<int>("earliest")) / 60000M, 3),
            WattMinutes = row.GetValue<int>("latest") - row.GetValue<int>("earliest")
        };
        
        return Ok(data);
    }
}