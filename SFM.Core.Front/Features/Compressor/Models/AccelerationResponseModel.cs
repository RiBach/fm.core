namespace SFM.Core.Front.Features.Compressor.Models;

public class AccelerationResponseModel
{
    public DateTime? Timestamp { get; set; }
    
    public int? X { get; set; }
    
    public int? Y { get; set; }
    
    public int? Z { get; set; }
}