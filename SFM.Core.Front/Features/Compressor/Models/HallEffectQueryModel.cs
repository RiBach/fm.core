using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Front.Features.Compressor.Models;

public class HallEffectQueryModel
{
    [Required] 
    public string StartTime { get; set; } = default!;
    
    [Required]
    public string EndTime { get; set; } = default!;
}