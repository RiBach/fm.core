using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Front.Features.Compressor.Models;

public class AccelerationQueryModel
{
    [Required] 
    public string StartTime { get; set; } = default!;
    
    [Required]
    public string EndTime { get; set; } = default!;
}