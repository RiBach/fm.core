namespace SFM.Core.Front.Features.Compressor.Models;

public class HallEffectResponseModel
{
    public DateTime? Timestamp { get; set; }
    
    public int? Value { get; set; }
}