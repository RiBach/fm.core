using Microsoft.AspNetCore.Mvc;
using SFM.Core.Front.CassandraDB;
using SFM.Core.Front.Features.Compressor.Models;

namespace SFM.Core.Front.Features.Compressor;

public class CompressorController(CassandraConnector cassandra) : Controller
{
    [HttpGet("acceleration")]
    public async Task<IActionResult> GetAccelerationByRange([FromQuery] AccelerationQueryModel model)
    {
        var statement = Queries.AccelerationByRange(model.StartTime, model.EndTime);
        
        var resultSet = await cassandra.Session!.ExecuteAsync(statement);
        var rows = resultSet.GetRows().ToList();

        if (rows is [null])
        {
            return NotFound();
        }
        
        var data = rows.Select(row => new AccelerationResponseModel
        {
            Timestamp = row.GetValue<DateTime>("timestamp"),
            X = row.GetValue<int>("acc_x"),
            Y = row.GetValue<int>("acc_y"),
            Z = row.GetValue<int>("acc_z")
        }).ToList();
        
        return Ok(data);
    }
    
    [HttpGet("mag")]
    public async Task<IActionResult> GetHallEffectByRange([FromQuery] HallEffectQueryModel model)
    {
        var statement = Queries.HallEffectByRange(model.StartTime, model.EndTime);
        
        var resultSet = await cassandra.Session!.ExecuteAsync(statement);
        var rows = resultSet.GetRows().ToList();
        
        if (rows is [null])
        {
            return NotFound();
        }
        
        var data = rows.Select(row => new HallEffectResponseModel
        {
            Timestamp = row.GetValue<DateTime>("timestamp"),
            Value = row.GetValue<int>("hall")
        }).ToList();
        
        return Ok(data);
    }
}