using Cassandra;
using Microsoft.AspNetCore.Mvc;
using SFM.Core.Front.CassandraDB;
using SFM.Core.Front.Features.TemperatureHumidity.Models;

namespace SFM.Core.Front.Features.TemperatureHumidity;

public class TemperatureHumidityController(CassandraConnector cassandra) : Controller
{
    [HttpGet("latest/battery")]
    public async Task<IActionResult> GetBattery()
    {
        List<Row?> rows = [];
        
        foreach (var identifier in EnvironmentSensorIdentifierDictionary.GetIdentifier.Values)
        {
            var statement = Queries.BatteryLatest($"{identifier}_battery");
            var resultSet = await cassandra.Session!.ExecuteAsync(statement);
            rows.Add(resultSet.GetRows().FirstOrDefault());
        }
        
        if (rows is [null])
        {
            return NotFound();
        }
        
        var data = rows.Select(row => new BatteryResponseModel
        {
            Identifier = row!.GetValue<string>("identifier"),
            SensorId = row.GetValue<string>("sensor_id"),
            Value = row.GetValue<short>("battery")
        }).ToList();

        return Ok(data);
    }
    
    [HttpGet("range/temperature")]
    public async Task<IActionResult> GetTemperature([FromQuery] TemperatureQueryModel model)
    {
        var statement = Queries.TemperatureByRange($"{model.Identifier}_temperature", model.StartTime, model.EndTime);
        
        var resultSet = await cassandra.Session!.ExecuteAsync(statement);
        var rows = resultSet.GetRows().ToList();
        
        if (rows is [null])
        {
            return NotFound();
        }
        
        var data = rows.Select(row => new TemperatureResponseModel
        {
            Identifier = row.GetValue<string>("identifier"),
            SensorId = row.GetValue<string>("sensor_id"),
            Timestamp = row.GetValue<DateTime>("timestamp"),
            Value = row.GetValue<short>("temperature")
        }).ToList();
        
        return Ok(data);
    }

    [HttpGet("latest/temperature")]
    public async Task<IActionResult> GetTemperatureLatest()
    {
        List<Row?> rows = [];
        
        foreach (var identifier in EnvironmentSensorIdentifierDictionary.GetIdentifier.Values)
        {
            var statement = Queries.TemperatureLatest($"{identifier}_temperature");
            var resultSet = await cassandra.Session!.ExecuteAsync(statement);
            rows.Add(resultSet.GetRows().FirstOrDefault());
        }
        
        if (rows is [null])
        {
            return NotFound();
        }
        
        var data = rows.Select(row => new TemperatureResponseModel
        {
            Identifier = row!.GetValue<string>("identifier"),
            SensorId = row.GetValue<string>("sensor_id"),
            Timestamp = row.GetValue<DateTime>("timestamp"),
            Value = row.GetValue<short>("temperature")
        }).ToList();
        
        return Ok(data);
    }
    
    [HttpGet("range/humidity")]
    public async Task<IActionResult> GetHumidity([FromQuery] HumidityQueryModel model)
    {
        var statement = Queries.HumidityByRange(model.Identifier, model.StartTime, model.EndTime);
        
        var resultSet = await cassandra.Session!.ExecuteAsync(statement);
        var rows = resultSet.GetRows().ToList();
        
        if (rows is [null])
        {
            return NotFound();
        }
        
        var data = rows.Select(row => new HumidityResponseModel
        {
            Identifier = row.GetValue<string>("identifier"),
            SensorId = row.GetValue<string>("sensor_id"),
            Timestamp = row.GetValue<DateTime>("timestamp"),
            Value = row.GetValue<short>("humidity")
        }).ToList();
        
        return Ok(data);
    }
    
    [HttpGet("latest/humidity")]
    public async Task<IActionResult> GetHumidityLatest()
    {
        List<Row?> rows = [];
        
        foreach (var identifier in EnvironmentSensorIdentifierDictionary.GetIdentifier.Values)
        {
            var statement = Queries.HumidityLatest($"{identifier}_humidity");
            var resultSet = await cassandra.Session!.ExecuteAsync(statement);
            rows.Add(resultSet.GetRows().FirstOrDefault());
        }
        
        if (rows is [null])
        {
            return NotFound();
        }
        
        var data = rows.Select(row => new HumidityResponseModel
        {
            Identifier = row!.GetValue<string>("identifier"),
            SensorId = row.GetValue<string>("sensor_id"),
            Timestamp = row.GetValue<DateTime>("timestamp"),
            Value = row.GetValue<short>("humidity")
        }).ToList();
        
        return Ok(data);
    }
}