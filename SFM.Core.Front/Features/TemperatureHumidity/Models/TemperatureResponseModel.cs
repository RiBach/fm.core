namespace SFM.Core.Front.Features.TemperatureHumidity.Models;

public class TemperatureResponseModel
{
    public string Identifier { get; set; } = default!;
    public string SensorId { get; set; } = default!;
    public DateTime Timestamp { get; set; }
    public short Value { get; set; }
}