namespace SFM.Core.Front.Features.TemperatureHumidity.Models;

public class BatteryResponseModel
{
    public string Identifier { get; set; } = default!;
    public string SensorId { get; set; } = default!;
    public short Value { get; set; } = default!;
}