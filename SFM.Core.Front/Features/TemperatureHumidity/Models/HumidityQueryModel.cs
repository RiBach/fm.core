using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Front.Features.TemperatureHumidity.Models;

public class HumidityQueryModel
{
    [Required]
    public string Identifier { get; set; } = default!;
    
    [Required] 
    public string StartTime { get; set; } = default!;
    
    [Required]
    public string EndTime { get; set; } = default!;
}