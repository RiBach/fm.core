using System.ComponentModel.DataAnnotations;

namespace SFM.Core.Front.Features.TemperatureHumidity.Models;

public class TemperatureQueryModel
{
    [Required]
    public required string Identifier { get; set; } = default!;
    
    [Required] 
    public required string StartTime { get; set; } = default!;
    
    [Required]
    public required string EndTime { get; set; } = default!;
}