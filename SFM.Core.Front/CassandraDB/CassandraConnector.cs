using Cassandra;
using ISession = Cassandra.ISession;

namespace SFM.Core.Front.CassandraDB;

public class CassandraConnector(IConfiguration conf)
{
    public ISession? Session;
    
    private readonly Cluster _cluster = Cluster.Builder()
        .AddContactPoint(conf["Cassandra:ContactPoint"])
        .WithDefaultKeyspace(conf["Cassandra:Keyspace"])
        .Build();
    
    
    public async Task ConnectAsync()
    {
        while (Session is null)
        {
            try
            {
                Console.WriteLine($@"{DateTime.Now.TimeOfDay:hh\:mm\:ss} : Connecting to Cassandra...");
                Session = await _cluster.ConnectAsync();
            }
            catch (Exception)
            {
                Console.WriteLine($@"{DateTime.Now.TimeOfDay:hh\:mm\:ss} : Cassandra not ready");
                await Task.Delay(TimeSpan.FromSeconds(15));
            }
        }
        
        Console.WriteLine($@"{DateTime.Now.TimeOfDay:hh\:mm\:ss} : Connected to Cassandra");
    }
}