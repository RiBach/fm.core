using Cassandra;

namespace SFM.Core.Front.CassandraDB;

public static class Queries
{
    #region Compressor
    public static SimpleStatement AccelerationByRange(string start, string end) => 
        new($"select timestamp, acc_x, acc_y, acc_z from compressor where identifier = 'imu' and timestamp >= '{start}' and timestamp < '{end}'");

    public static SimpleStatement HallEffectByRange(string start, string end) => 
        new($"select timestamp, hall from compressor where identifier = 'hall_effect' and timestamp >= '{start}' and timestamp < '{end}'");

    #endregion

    #region Energy
    public static SimpleStatement PowerLatest() => 
        new($"select timestamp, power from energy where identifier = 'power' order by timestamp desc limit 1");

    
    public static SimpleStatement PowerByRange(string start, string end) => 
        new($"select timestamp, power from energy where identifier = 'power' and timestamp >= '{start}' and timestamp <= '{end}'");
    
    //TODO: Rethink this min max thing because restarting the plug can cause the min to be higher than the max without a tell that it reset
    public static SimpleStatement EnergyByRange(string start, string end) =>
        new($"select min(energy) as earliest, max(energy) as latest from energy where identifier = 'energy' and timestamp >= '{start}' and timestamp <= '{end}'");
    
    #endregion

    #region TemperatureHumidity
    public static SimpleStatement BatteryLatest(string identifier) => 
        new($"select identifier, sensor_id, timestamp, battery from environment where identifier = '{identifier}' order by timestamp desc limit 1");
    
    public static SimpleStatement HumidityLatest(string identifier) =>
        new($"select identifier, sensor_id, timestamp, humidity from environment where identifier = '{identifier}' order by timestamp desc limit 1");
    
    public static SimpleStatement TemperatureLatest(string identifier) => 
        new($"select identifier, sensor_id, timestamp, temperature from environment where identifier = '{identifier}' order by timestamp desc limit 1");
    
    
    public static SimpleStatement HumidityByRange(string identifier, string start, string end) => 
        new($"select identifier, sensor_id, timestamp, humidity from environment where identifier = '{identifier}' and timestamp >= '{start}' and timestamp < '{end}'");
    
    public static SimpleStatement TemperatureByRange(string identifier, string start, string end) => 
        new($"select identifier, sensor_id, timestamp, temperature from environment where identifier = '{identifier}' and timestamp >= '{start}' and timestamp < '{end}'");

    #endregion
}