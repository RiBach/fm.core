namespace SFM.Core.Front.CassandraDB;

public static class EnvironmentSensorIdentifierDictionary
{
    /// <summary>
    /// Dictionary to get the identifier for the environment sensor.
    /// Key: SensorId
    /// </summary>
    public static readonly Dictionary<string, string> GetIdentifier = new()
    {
        { "th_a4c1384bba230fc2", "lower" },
        { "th_a4c1381c3f2a8080", "upper" },
        { "th_a4c13870d8c959a0", "outer" }
    };
}